<?php


class ModelSepatu extends CI_Model
{
	var $table = "sepatu";
	var $primaryKey = "id_sepatu";
	var $barcode = "kode_sepatu";
	public function insertData($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function getAll()
	{
		$this->db->where("is_active", 1);
		return $this->db->get($this->table)->result();
	}

	public function getByPrimaryKey($id)
	{
		$this->db->where($this->primaryKey, $id);
		return $this->db->get($this->table)->row();
	}

	public function updateData($id, $data)
	{
		$this->db->where($this->primaryKey, $id);
		return $this->db->update($this->table, $data);
	}

	public function deleteData($id)
	{
		$this->db->where($this->primaryKey, $id);
		return $this->db->delete($this->table);
	}
	public function get($id = null){
		$this->db->select('sepatu.*, nama_sepatu');
		$this->db->from('sepatu');
		if ($id != null){
			$this->db->where('id_sepatu', $id);
		}
		$query = $this->db->get();
		return $query;
	}
	function jum_buku_perjudul(){
		$this->db->group_by('nama_sepatu');
		$this->db->select('merk_sepatu');
		$this->db->select('count(*) as stock');
		return $this->db->from('sepatu')->get()->result();
	}
}
