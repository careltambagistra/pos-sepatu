<?php


class ModelTransaksi extends CI_Model {
	var $table = "transaksi";
	var $primaryKey = "id_transaksi";

	public function insert($data) {
		return $this->db->insert($this->table, $data);
	}

	public function insertGetId($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function getAll() {
		return $this->db->get($this->table)->result();
	}

	public function getByPrimaryKey($id) {
		$this->db->where($this->primaryKey, $id);
		return $this->db->get($this->table)->row();
	}

	public function update($id, $data) {
		$this->db->where($this->primaryKey, $id);
		return $this->db->update($this->table, $data);
	}

	// delete data
	public function delete($id) {

		return $this->db->update($this->table, array("is_active" => 0));
	}

	public function get($id = null){
		$this->db->select('transaksi.*, no_transaksi');
		$this->db->from('transaksi');
		if ($id != null){
			$this->db->where('id_transaksi', $id);
		}
		$query = $this->db->get();
		return $query;
	}
}
