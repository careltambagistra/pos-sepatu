<?php


class ModelPegawai extends CI_Model
{
	var $table = "pegawai";
	var $primaryKey = "id_pegawai";

	public function insertData($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function getAll()
	{
		return $this->db->get($this->table)->result();
	}

	public function getByPrimaryKey($id)
	{
		$this->db->where($this->primaryKey, $id);
		return $this->db->get($this->table)->row();
	}

	public function updateData($id, $data)
	{
		$this->db->where($this->primaryKey, $id);
		return $this->db->update($this->table, $data);
	}

	public function deleteData($id)
	{
		$this->db->where($this->primaryKey, $id);
		return $this->db->delete($this->table);
	}
	public function get($id = null){
		$this->db->select('pegawai.*, nama_pegawai');
		$this->db->from('pegawai');
		if ($id != null){
			$this->db->where('id_pegawai', $id);
		}
		$query = $this->db->get();
		return $query;
	}
}
