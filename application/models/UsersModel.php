<?php


class UsersModel extends CI_Model
{
	public function getByEmail($email)
	{
		$this->db->where("email", $email);
		return $this->db->get("users")->row();
	}

	public function getByNamaAndPassword($nama, $password)
	{
		$where = array(
			"nama" => $nama,
			"password" => md5($password)
		);
		$this->db->where($where);
		return $this->db->get("users")->row();
	}

	public function insert($data)
	{
		$this->db->insert("users", $data);
	}

	public function update($id, $data)
	{
		$this->db->where("id", $id);
		$this->db->update("users", $data);
	}

	public function getByToken($token)
	{
		$this->db->where("token", $token);
		return $this->db->get("users")->row();

	}

}
