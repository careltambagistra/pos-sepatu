create database pos_sepatu;
use pos_sepatu;

create table sepatu(
  id_sepatu int not null primary key auto_increment,
  kode_sepatu varchar(20) not null,
  nama_sepatu varchar(200),
  merk_sepatu varchar(200),
  description varchar(500),
  harga int,
  stock int,
  is_active tinyint default 1,
  gambar_sepatu text,
  unique(kode_sepatu)
);

create table pegawai(
  id_pegawai int not null primary key auto_increment,
  nama_pegawai varchar(100),
  tanggal_lahir date,
  alamat varchar(100),
  jenis_kelamin varchar(15),
  posisi varchar(20)
);

create table users(
    id int not null primary key auto_increment,
    email varchar(40) not null,
    nama varchar(40),
    password varchar(200),
    role enum("Superadmin","Admin","user") default "user",
    token varchar(200),
    is_active tinyint default 0
);

CREATE TABLE IF NOT EXISTS ci_sessions (
        id varchar(128) NOT NULL,
        ip_address varchar(45) NOT NULL,
        timestamp int(10) unsigned DEFAULT 0 NOT NULL,
        data blob NOT NULL,
        KEY ci_sessions_timestamp (timestamp)
);

create table transaksi(
  id_transaksi int not null primary key auto_increment,
  no_transaksi varchar(50),
  tanggal_transaksi datetime
);

create table item_transaksi(
  id_item_transaksi int not null primary key auto_increment,
  harga_item_transaksi int,
  qty_item_transaksi int,
  total_item_transaksi int,
  id_sepatu int,
  id_transaksi int,
  foreign key(id_transaksi) references transaksi(id_transaksi),
  foreign key(id_sepatu) references sepatu(id_sepatu)
);

alter table transaksi add nomor int default 0;
alter table pegawai add foto_pegawai text;
