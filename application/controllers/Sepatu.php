<?php


class Sepatu extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("ModelSepatu");
	}

	public function index()
	{
		$listSepatu = $this->ModelSepatu->getAll();
		$data = array(
			"header" => "Daftar Sepatu",
			"sepatus" => $listSepatu,
			"page" => "sepatu/listSepatu"
		);
		$this->load->view("layout/dashboard", $data);
	}

	function getSepatu()
	{
		$kode = $this->input->post('kode_sepatu');
		$data = $this->ModelSepatu->getDataBukuBykode($kode);
		echo json_encode($data);
	}

	private function _do_upload()
	{
		$config['upload_path'] = 'asset/images/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = '50000';
		//$config['file_name'] = 'barang-' . date('ymd') . '-' . substr(md5(rand()), 0, 100000);

		$this->load->library('upload', $config);

		$B = $this->input->post(null, TRUE);
		if (!$this->upload->do_upload('gambar')) {
			$this->session->set_flashdata('msg', $this->upload->display_errors('', ''));
			redirect('sepatu');
		}
	}

	public function detailSepatu($idSepatu)
	{
		$sepatu = $this->ModelSepatu->getByPrimaryKey($idSepatu);
		$data = array(
			"header" => "Detail Sepatu",
			"sepatu" => $sepatu,
			"page" => "sepatu/detailSepatu"
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function buatSepatu()
	{
		$data = array(
			"header" => "Tambah Sepatu",
			"page" => "sepatu/buatSepatu"
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function prosesBuat()
	{
		$config['upload_path'] = 'asset/images/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = '50000';
		$this->load->library('upload', $config);
		$B = $this->input->post(null, TRUE);

		if (@$_FILES['gambar']['name'] != null) {
			if ($this->upload->do_upload('gambar')) {
				$C = $B['gambar'] = $this->upload->data('file_name');
				$sepatu = array(
					"kode_sepatu" => $this->input->post("barcode"),
					"nama_sepatu" => $this->input->post("nama"),
					"merk_sepatu" => $this->input->post("merk"),
					"description" => $this->input->post("description"),
					"harga" => $this->input->post("harga"),
					"stock" => $this->input->post("stock"),
					"gambar_sepatu" => $C
				);
				$this->ModelSepatu->insertData($sepatu);
				redirect("sepatu");
			}
		}
	}

	public function editSepatu($idSepatu)
	{
		$sepatu = $this->ModelSepatu->getByPrimaryKey($idSepatu);
		$data = array(
			"header" => "Edit Sepatu",
			"page" => "sepatu/editSepatu",
			"sepatu" => $sepatu
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function prosesEdit()
	{
		$config['upload_path'] = 'asset/images/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = '5000';
		$this->load->library('upload', $config);
		$B = $this->input->post(null, TRUE);

		if (@$_FILES['gambar_sepatu']['name'] != null) {
			if ($this->upload->do_upload('gambar_sepatu')) {
				$G = $B['gambar_sepatu'] = $this->upload->data('file_name');
				$id = $this->input->post("id");
				$sepatu = array(
					"kode_sepatu" => $this->input->post("barcode"),
					"nama_sepatu" => $this->input->post("nama"),
					"merk_sepatu" => $this->input->post("merk"),
					"description" => $this->input->post("description"),
					"harga" => $this->input->post("harga"),
					"stock" => $this->input->post("stock"),
					"gambar_sepatu" => $G
				);
				$this->ModelSepatu->updateData($id, $sepatu);
				redirect("sepatu");
			}
		}
	}

	public function prosesDelete()
	{
		$id = $this->input->post("id");
		$this->ModelBuku->deleteData($id);
		redirect("sepatu");
	}
}
