<?php


class Pegawai extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("ModelPegawai");
	}

	public function index()
	{
		$listPegawai = $this->ModelPegawai->getAll();
		$data = array(
			"header" => "Daftar Pegawai",
			"pegawais" => $listPegawai,
			"page" => "pegawai/listPegawai"
		);
		$this->load->view("layout/dashboard", $data);
	}

	private function _do_upload()
	{
		$config['upload_path'] = 'asset/images/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = '5000';
		//$config['file_name'] = 'barang-' . date('ymd') . '-' . substr(md5(rand()), 0, 100000);

		$this->load->library('upload', $config);

		$B = $this->input->post(null, TRUE);
		if (!$this->upload->do_upload('foto')) {
			$this->session->set_flashdata('msg', $this->upload->display_errors('', ''));
			redirect('pegawai');
		}
	}

	public function detailPegawai($idPegawai)
	{
		$pegawai = $this->ModelPegawai->getByPrimaryKey($idPegawai);
		$data = array(
			"header" => "Detail Pegawai",
			"pegawai" => $pegawai,
			"page" => "pegawai/detailPegawai"
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function tambahPegawai()
	{
		$data = array(
			"header" => "Tambah Pegawai",
			"page" => "pegawai/tambahPegawai"
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function prosesBuat()
	{
		$config['upload_path'] = 'asset/images/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = '5000';
		$this->load->library('upload', $config);
		$B = $this->input->post(null, TRUE);

		if (@$_FILES['foto']['name'] != null) {
			if ($this->upload->do_upload('foto')) {
				$G = $B['foto'] = $this->upload->data('file_name');
				$pegawai = array(
					"nama_pegawai" => $this->input->post("nama"),
					"tanggal_lahir" => $this->input->post("tanggal"),
					"alamat" => $this->input->post("alamat"),
					"jenis_kelamin" => $this->input->post("kelamin"),
					"posisi" => $this->input->post("posisi"),
					"foto_pegawai" => $G
				);
				$this->ModelPegawai->insertData($pegawai);
				redirect("pegawai");
			}
		}
	}

	public function updatePegawai($idPegawai)
	{
		$pegawai = $this->ModelPegawai->getByPrimaryKey($idPegawai);
		$data = array(
			"header" => "Edit Pegawai",
			"page" => "pegawai/editPegawai",
			"pegawai" => $pegawai
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function prosesEdit()
	{
		$config['upload_path'] = 'asset/images/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = '50000';
		$this->load->library('upload', $config);
		$B = $this->input->post(null, TRUE);

		if (@$_FILES['foto']['name'] != null) {
			if ($this->upload->do_upload('foto')) {
				$G = $B['foto'] = $this->upload->data('file_name');

				$id = $this->input->post("id");
				$pegawai = array(
					"nama_pegawai" => $this->input->post("nama"),
					"tanggal_lahir" => $this->input->post("tanggal"),
					"alamat" => $this->input->post("alamat"),
					"jenis_kelamin" => $this->input->post("kelamin"),
					"posisi" => $this->input->post("posisi"),
					"foto_pegawai" => $G
				);
				$this->ModelPegawai->updateData($id, $pegawai);
				redirect("pegawai");
			}
		}
	}

	public function hapusPegawai($id)
	{
		$pegawai = $this->ModelPegawai->getByPrimaryKey($id);
		$data = array(
			"pegawai" => $pegawai
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function prosesDelete()
	{
		$id = $this->input->post("id");
		$this->ModelPegawai->deleteData($id);
		redirect("pegawai");
	}
}
