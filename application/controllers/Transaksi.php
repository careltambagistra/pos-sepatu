<?php


class Transaksi extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array("ModelSepatu", "ModelTransaksi", "ModelItemTransaksi"));
	}

	public function index() {
		$listTransaksi = $this->ModelTransaksi->getAll();
		$data = array(
			"header" => "Daftar Transaksi",
			"transaksis" => $listTransaksi,
			"page" => "transaksi/listTransaksi"
		);
		$this->load->view("layout/dashboard", $data);
	}
	public function detailTransaksi($idTransaksi)
	{
		$transaksi = $this->ModelItemTransaksi->getDetailTransaksi($idTransaksi);
		$data = array(
			"header" => "Detail Transaksi",
			"transaksis" => $transaksi,
			"page" => "transaksi/detailTransaksi"
		);
		$this->load->view("layout/dashboard", $data);
	}
}
