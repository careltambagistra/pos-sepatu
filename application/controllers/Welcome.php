<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('ModelSepatu');
	}
	public function index()
	{
		$chart = $this->ModelSepatu->getAll();
		$data = array(
			"header" => "Dashboard",
			"hasil" => $chart,
			"page" => "dashboard"
		);
		$this->load->view('layout/dashboard', $data);
	}
}
