<div class="col-md-12">
	<div class="card card-gray-dark">
		<div class="card-header">
			<h3 class="card-title">*</h3>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<table id="example2" class="table table-bordered table-hover">
				<thead>
				<tr>
					<th id="hidden">ID</th>
					<th>Nomor Transaksi</th>
					<th>Tanggal Transaksi</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$no = 1;
				foreach ($transaksis as $transaksi) {
					?>
					<tr>
						<td><?= $no++ ?></td>
						<td><?= $transaksi->no_transaksi ?></td>
						<td><?= $transaksi->tanggal_transaksi ?></td>
						<td>
							<a href="<?= site_url('transaksi/detailTransaksi/') . $transaksi->id_transaksi ?>"
							   class="btn btn-sm bg-gradient-fuchsia">
								<i class="fas fa-eye"></i></a>
						</td>
					</tr>
					<?php
				}
				?>
				</tbody>
			</table>
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>
