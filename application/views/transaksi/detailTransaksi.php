<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<table class="table">
					<thead>
					<tr>
						<th>#</th>
						<th>Nama Sepatu</th>
						<th>Harga Sepatu</th>
						<th>Jumlah</th>
						<th>Total</th>
					</tr>
					</thead>
					<tbody>
					<?php
					$no = 1;
					$totalSemua = 0;
					foreach ($transaksis as $item) {
						$totalHarga =(int)$item->qty_item_transaksi*(int)$item->harga_item_transaksi;
						$totalSemua += $totalHarga;
						?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?= $item->nama_sepatu ?></td>
							<td><?= formatRupiah($item->qty_item_transaksi) ?></td>
							<td><?= $item->harga_item_transaksi ?></td>
							<td><?= formatRupiah($totalHarga) ?></td>
						</tr>
					<?php } ?>
					<tr>
						<td colspan="4"><b>Total</b></td>
						<td align="left"><?= formatRupiah($totalSemua) ?></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="card-footer">
				<button id="print-pdf" type="button" class="btn btn-sm bg-gradient-cyan">
					Print PDF
				</button>
			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$("#print-pdf").on("click",function () {
			window.print()
		})
	})
</script>
