<div class="col-md-3">
	<div class="card card-gray-dark">
		<div class="card-header">
			<h3 class="card-title">Barcode Generator</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<div class="card-body">
			<?= $sepatu->kode_sepatu?>
			<?php
			$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
			echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($sepatu->kode_sepatu, $generator::TYPE_CODE_128)) . '">';
			?>
		</div>
	</div>
</div>
