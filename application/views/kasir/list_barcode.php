<div class="col-md-3">
	<div class="card card-gray-dark">
		<div class="card-header">
			<h3 class="card-title">Barcode Generator</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
			<div>
				<button id="print-pdf" type="button" class="btn btn-sm bg-gradient-cyan">
					Print PDF
				</button>
			</div>
		</div>
		<div class="card-body">
			<?php
			$no = 1;
			foreach ($sepatus as $sepatu) {
			?>
			<p><?= $sepatu->kode_sepatu ?></p>
			<p>
				<?php
				$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
				echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($sepatu->kode_barcode, $generator::TYPE_CODE_128)) . '">';
				}
				?>
			</p>
		</div>
	</div>
</div>
<script>
	$(function () {
		$("#print-pdf").on("click",function () {
			window.print()
		})
	})
</script>
