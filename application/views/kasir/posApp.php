<div class="row">
	<div class="col-md-12">
		<div class="card card-gray-dark">
			<div class="card-header separator">
				<h4>Scan Barcode</h4>
			</div>
			<div class="card-body">
				<div class="form-group">
					<select name="" class="form-control" id="select-buku">
						<option value="" disabled selected>Scan</option>
						<?php
						foreach ($sepatus as $b) {
							echo "<option data-judul='$b->nama_sepatu' "
									. "data-harga='$b->harga' "
									. "value='$b->id_sepatu'> "
									. "$b->kode_sepatu / $b->nama_sepatu"
									. "</option>";
						}
						?>
					</select>
				</div>
				<input readonly type="hidden" id="judul-buku" class="form-control"/>
				<input readonly type="hidden" id="harga-buku" class="form-control"/>
				<label>Jumlah Sepatu</label>
				<div class="form-group">
					<input class="" id="jumlah-buku" type="number"/>
				</div>
				<div class="form-group">
					<button type="button" class="btn btn-primary float-right" id="btn-add-item"><i
								class="fas fa-plus"></i></button>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="card card-gray-dark">
			<div class="card-header separator">
				<h4>Sepatu yang dibeli</h4>
			</div>
			<div class="card-body">
				<table class="table" id="table-transaksi">
					<thead>
					<tr>
						<th>Nama Sepatu</th>
						<th>Harga</th>
						<th>Jumlah</th>
						<th>Sub Total</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
			<div class="card-footer">
				<button class="btn btn-primary" id="btn-save-transaksi">
					<i class="fa fa-save"></i> Simpan
				</button>
			</div>
		</div>
	</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
<script>
	$(function () {
		let buku;
		$("#select-buku")
				.select2()
				.on("change", function () {
					var optionSelected = $(this).children("option:selected");
					$("#judul-buku").val(optionSelected.data("judul"));
					$("#harga-buku").val(optionSelected.data("harga"));
					$("#jumlah-buku").val(1);
				})
		$("#btn-add-item").on("click", function () {
			let id = $("#select-buku").val();
			let judulBuku = $("#judul-buku").val();
			let hargaBuku = $("#harga-buku").val();
			let jumlahBuku = $("#jumlah-buku").val();
			let subTotal = parseInt(hargaBuku) * parseInt(jumlahBuku);
			if (judulBuku != "") {
				let tr = `<tr data-id="${id}">`;
				tr += `<td>${judulBuku}</td>`;
				tr += `<td>${hargaBuku}</td>`;
				tr += `<td>${jumlahBuku}</td>`;
				tr += `<td>${subTotal}</td>`;
				tr += `<td>`;
				tr += `<button class="btn btn-xs btn-del-item btn-danger"> <i class="fas fa-trash"></i></button>`;
				tr += `</td>`;
				tr += `</tr>`;
				$("#table-transaksi tbody").append(tr);
				$("#select-buku").val("").trigger("change");
				$("#kode-barcode").val();
				$("#judul-buku").val();
				$("#harga-buku").val();
				$("#jumlah-buku").val(1);
				$(".btn-del-item").on("click", function () {
					$(this).parent().parent().remove();
				});
			}
		});
		$("#btn-save-transaksi").on("click", function () {
			let rows = $("#table-transaksi tbody tr");
			let itemTransaksi = [];
			rows.each(function () {
				let row = $(this);
				let item = {
					id_sepatu: row.data("id"),
					harga_item_transaksi: row.children().eq(2).text(),
					qty_item_transaksi: row.children().eq(3).text(),
					total_item_transaksi: row.children().eq(4).text(),
				};
				itemTransaksi.push(item);
			});
			let dataKirim = JSON.stringify(itemTransaksi);
			$(function () {
				swal.fire({
					title: "Processing Data..",
					text: "Data sedang berkelana",
					imageUrl: '<?= base_url() ?>' + "asset/images/loading.gif",
					showConfirmButton: false,
					allowOutsideClick: false
				});
				$.ajax({
					url: window.base_url + "kasir/prosesTransaksi",
					type: "POST",
					data: {
						item_transaksi: dataKirim

					},
					success: function (result) {
						if (parseInt(result) > 0) {
							//success
							window.location.replace(window.base_url+"kasir");
						} else {
							//error
							swal.fire("Transaksi Gagal", result,"ERROR");
						}
					}
				});
			});
		})
	})
</script>
