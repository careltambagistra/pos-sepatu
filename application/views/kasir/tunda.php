<div class="row">
	<div class="col-md-12">
		<div class="card card-gray-dark">
			<div class="card-header separator">
				<h4>Buku yang dibeli</h4>
			</div>
			<div class="card-body">
				<table class="table" id="table-transaksi">
					<thead>
					<tr>
						<th>Judul Buku</th>
						<th>Harga</th>
						<th>Jumlah</th>
						<th>Sub Total</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="card-footer">
				<button class="btn btn-primary" id="btn-save-transaksi">
					<i class="fa fa-save"></i> Lanjut
				</button>
			</div>
		</div>
	</div>
</div>
