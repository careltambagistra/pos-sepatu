<div class="col-md-12">
	<div class="card card-gray-dark">
		<div class="card-header">
			<h3 class="card-title">*</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<div class="card-body">
			<form action="<?= site_url('sepatu/prosesEdit')?>" enctype="multipart/form-data" method="post">
				<div class="form-group">
					<label for="kode_barcode">Kode Barcode</label>
					<input type="text" id="kode_barcode" name="barcode" class="form-control" value="<?= $sepatu->kode_sepatu ?>">
				</div>
				<div class="form-group">
					<label for="judul_buku">Nama Sepatu</label>
					<input type="text" id="nama_sepatu" name="nama" class="form-control" value="<?= $sepatu->nama_sepatu ?>">
				</div>
				<div class="form-group">
					<label for="pencipta_buku">Merk Sepatu</label>
					<input type="text" id="merk_sepatu" name="merk" class="form-control" value="<?= $sepatu->merk_sepatu ?>">
				</div>
				<div class="form-group">
					<label for="description">Description</label>
					<input id="description" name="description" class="form-control" value="<?= $sepatu->description ?>">
				</div>
				<div class="form-group">
					<label for="harga">Harga</label>
					<input type="number" id="harga" name="harga" class="form-control" value="<?= $sepatu->harga ?>">
				</div>
				<div class="form-group">
					<label for="stock">Stock</label>
					<input type="number" id="stock" name="stock" class="form-control" value="<?= $sepatu->stock ?>">
				</div>
				<div class="form-group ">
					<div class="col-sm-2">Gambar</div>
					<div class="clo-sm-10">
						<div class="row">
							<div class="col-sm-3">
								<img src="<?= base_url('asset/images/') . $sepatu->gambar_sepatu; ?>" class="img-thumbnail">
							</div>
							<div class="form-group col-sm-9">
								<label for="stock-barang">Gambar Sepatu</label>
								<input value="<?= $sepatu->gambar_sepatu ?>" required type="file" name="gambar_sepatu" id="gambar_sepatu" class="form-control"/>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<a href="<?= site_url('sepatu') ?>" class="btn btn-secondary">Cancel</a>
					<input type="submit" value="Simpan" class="btn btn-success float-right">
				</div>
				<input type="hidden" id="id_sepatu" name="id" value="<?= $sepatu->id_sepatu ?>">
			</form>
		</div>
	</div>
