<div class="col-md-12">
	<div class="card card-gray-dark">
		<div class="card-header">
			<h3 class="card-title">*</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<div class="card-body">
			<form id="form-tambah-buku" enctype="multipart/form-data" action="<?= site_url('sepatu/prosesBuat') ?>" method="post">
				<div class="form-group">
					<label for="kode_barcode">Kode Barcode</label>
					<input autocomplete="off" type="text" id="kode_sepatu" name="barcode" class="form-control"
						   required>
				</div>
				<div class="form-group">
					<label for="judul_buku">Nama Sepatu</label>
					<input autocomplete="off" type="text" id="nama_sepatu" name="nama" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="pencipta_buku">Merk Sepatu</label>
					<input autocomplete="off" type="text" id="merk_sepatu" name="merk" class="form-control"
						   required>
				</div>
				<div class="form-group">
					<label for="description">Description</label>
					<textarea autocomplete="off" id="description" name="description" class="form-control"
							  rows="4"></textarea>
				</div>
				<div class="form-group">
					<label for="harga">Harga</label>
					<input autocomplete="off" type="number" id="harga" name="harga" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="stock">Stock</label>
					<input autocomplete="off" type="number" id="stock" name="stock" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="gambar-buku">Gambar Sepatu</label>
					<input required type="file" name="gambar" id="gambar_sepatu" class="form-control"/>
				</div>
				<div class="col-12">
					<a href="<?= site_url('sepatu') ?>" class="btn btn-secondary">Cancel</a>
					<input type="submit" id="btn-save-buku" value="Buat Baru" class="btn btn-success float-right">
				</div>
			</form>
		</div>
	</div>
</div>
