<div class="col-md-12">
	<div class="card card-gray-dark">
		<div class="card-header">
			<h1 class="card-title">*</h1>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<table class="table table-bordered table-hover">
				<thead>
				<tr>
					<th id="hidden">No</th>
					<th>Barcode</th>
					<th>Nama Sepatu</th>
					<th>Merk Sepatu</th>
					<th>Harga</th>
					<th>Stock</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$no = 1;
				foreach ($sepatus as $sepatu) {
					?>
					<tr>
						<td><?= $no++ ?></td>
						<td>
							<?= $sepatu->kode_sepatu ?>
						</td>
						<td><?= $sepatu->nama_sepatu ?></td>
						<td><?= $sepatu->merk_sepatu ?></td>
						<td><?= formatRupiah($sepatu->harga) ?></td>
						<td><?= $sepatu->stock ?></td>
						<td>
							<a href="<?= site_url('sepatu/editSepatu/') . $sepatu->id_sepatu ?>"
							   class="btn btn-sm bg-gradient-green">
								<i class="fas fa-edit"></i></a>
							<a href="#" data-id="<?= $sepatu->id_sepatu ?>"
							   class="btn btn-sm bg-gradient-danger btn-delete-buku">
								<i class="fas fa-trash"></i></a>
							<a href="<?= site_url('sepatu/detailSepatu/') . $sepatu->id_sepatu ?>"
							   class="btn btn-sm bg-gradient-fuchsia">
								<i class="fas fa-eye"></i></a>
							<a href="<?= site_url('kasir/barcode/') . $sepatu->id_sepatu ?>"
							   class="btn btn-sm bg-gradient-cyan">
								<i class="fas fa-barcode"></i></a>
						</td>
					</tr>
					<?php
				}
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-barcode">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<?= $sepatu->kode_sepatu ?>
				<?php
				$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
				echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($sepatu->kode_sepatu, $generator::TYPE_CODE_128)) . '">';
				?>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-confirm-delete">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<h4>Anda yakin ingin hapus data sepatu ini?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn bg-gradient-green" data-dismiss="modal">Tidak</button>
				<button type="button" class="btn bg-gradient-danger" id="btn-delete">Hapus</button>
			</div>
		</div>
	</div>
</div>
<form id="form-delete" method="post" action="<?= site_url('sepatu/prosesDelete') ?>">
</form>
<script>
	$(function () {
		let idSepatu = 0;
		$(".btn-generate-barcode").on("click", function () {
			var idSepatu = $(this).data("id");
			console.log(idSepatu);
			$("#modal-barcode").modal("show");
		});
		$(".btn-delete-sepatu").on("click", function () {
			idSepatu = $(this).data("id");
			console.log(idSepatu);
			$("#modal-confirm-delete").modal("show");
		});
		$("#btn-delete").on("click", function () {
//panggil url untuk hapus data
			let inputId = $("<input>")
					.attr("type", "hidden")
					.attr("name", "id")
					.val(idBuku);
			let formDelete = $("#form-delete");
			formDelete.empty().append(inputId);
			formDelete.submit();
			$("#modal-confirm-delete").modal("hide");
		});
	});
</script>
