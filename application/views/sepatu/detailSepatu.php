<div class="row">
	<section class="col-md-4 connectedSortable">
		<div class="card card-gray-dark">
			<div class="card-header">
				<h3 class="card-title">
					<i class="fas fa-file-image mr-1"></i>
					Gambar
				</h3>
			</div><!-- /.card-header -->
			<div class="card-body">
				<div class="tab-content p-0">
					<div class="col-lg-auto">
						<img src="<?= base_url('asset/images/') . $sepatu->gambar_sepatu; ?>" class="img-thumbnail">
					</div>
				</div>
			</div><!-- /.card-body -->
		</div>
	</section>
	<section class="col-md-8 connectedSortable">
		<!-- Map card -->
		<div class="card bg-gradient-white">
			<div class="card-header border-0">
				<div class="card-footer">
					<div class="row">
						<div class="col-10 text-middle">
							<b><h3><?= $sepatu->nama_sepatu ?></h3></b><br>
							<?= $sepatu->merk_sepatu ?><br>
							<?= formatRupiah($sepatu->harga) ?><br>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</section>
	<section class="col-md-12 connectedSortable">
		<!-- Map card -->
		<div class="card bg-gradient-white">
			<div class="card-header border-1">
				<div class="card-footer">
					<div class="row">
						<div class="col-lg text-middle">
							<b><h6><?= $sepatu->description ?></h6></b><br>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</section>
</div>
