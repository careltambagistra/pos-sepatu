<div class="row">
	<div class="col-sm">
		<div class="info-box">
			<span class="info-box-icon bg-info"><i class="fas fa-book"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">Sepatu</span>
				<span class="info-box-number"><?= $this->fungsi->count_buku()?></span>
			</div>
			<!-- /.info-box-content -->
		</div>
		<!-- /.info-box -->
	</div>
	<!-- /.col -->
	<div class="col-md">
		<div class="info-box">
			<span class="info-box-icon bg-success"><i class="fas fa-person-booth"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">Pegawai</span>
				<span class="info-box-number"><?= $this->fungsi->count_pegawai()?></span>
			</div>
			<!-- /.info-box-content -->
		</div>
		<!-- /.info-box -->
	</div>
	<!-- /.col -->
	<div class="col-lg">
		<div class="info-box">
			<span class="info-box-icon bg-warning"><i class="fas fa-money-check-alt"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">Transaksi</span>
				<span class="info-box-number"><?= $this->fungsi->count_transaksi()?></span>
			</div>
			<!-- /.info-box-content -->
		</div>
		<!-- /.info-box -->
	</div>
</div>
<<<<<<< HEAD
=======
<script>
	var ctx = document.getElementById('myChart').getContext('2d');
	var chart = new Chart(ctx, {
		// The type of chart we want to create
		type: 'line',
		// The data for our dataset
		data: {
			labels: [<?php echo $kode_barcode; ?>],
			datasets: [{
				label:'Data Buku',
				backgroundColor: ['rgb(255, 99, 132)', 'rgba(56, 86, 255, 0.87)', 'rgb(60, 179, 113)','rgb(175, 238, 239)','rgb(42, 27, 38)','rgb(43, 47, 232)','rgb(224, 46, 39)','rgb(34, 49, 87)','rgb(981, 42, 75)','rgb(175, 624, 347)'],
				borderColor: ['rgb(255, 99, 132)'],
				data: [<?php echo $jumlah; ?>]
			}]
		},
		// Configuration options go here
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			}
		}
	});
</script>
>>>>>>> 56a9c9caff26b91f330d20bd2223a51dc2663626
