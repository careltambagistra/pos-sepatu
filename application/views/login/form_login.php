<div class="wrap-login100 p-l-55 p-r-55 p-t-25 p-b-25">
	<form action="<?= site_url("login/prosesLogin") ?>" method="post"
		  class="login100-form validate-form flex-sb flex-w">
					<span class="login100-form-title p-b-32">
						Masuk ke Akun Kasir Anda
					</span>
		<span class="txt1 p-b-11">
						Username
					</span>
		<div class="wrap-input100 validate-input m-b-36" data-validate="Username is required">
			<input autocomplete="off" class="input100" type="text" name="nama">
			<span class="focus-input100"></span>
		</div>
		<span class="txt1 p-b-11">
						Sandi
					</span>
		<div class="wrap-input100 validate-input m-b-12" data-validate="Password is required">
						<span class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
			<input class="input100" type="password" name="password">
			<span class="focus-input100"></span>
		</div>

		<div class="flex-sb-m w-full p-b-48">
			<div>
				<a href="<?= site_url("register") ?>" class="txt3">
					Belum punya akun ?
				</a>
			</div>
		</div>
		<div class="container-login100-form-btn">
			<button class="login100-form-btn">
				Masuk
			</button>
		</div>
	</form>
</div>
