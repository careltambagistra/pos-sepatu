<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Login Error</h1>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="error-page">
			<div class="error-content">
				<h3><i class="fa fa-exclamation-triangle text-danger"></i> Oops! Ada yang salah.</h3>

				<p>
					Username atau Password anda salah.
					Silakan <a href="<?= site_url("login") ?>">klik disini</a> untuk login kembali.
				</p>
			</div>
		</div>
		<!-- /.error-page -->

	</section>
	<!-- /.content -->
</div>
