<li class="nav-item has-treeview">
	<a href="<?= site_url('welcome') ?>" class="nav-link">
		<i class="nav-icon fas fa-tachometer-alt"></i>
		<p>
			Dashboard
		</p>
	</a>
</li>
<li class="nav-item has-treeview">
	<a href="<?= site_url('kasir') ?>" class="nav-link">
		<i class="nav-icon fas fa-calculator"></i>
		<p>
			Kasir
		</p>
	</a>
</li>
<li class="nav-item has-treeview">
	<a href="<?= site_url('transaksi') ?>" class="nav-link">
		<i class="nav-icon fas fa-money-check-alt"></i>
		<p>
			Transaksi
		</p>
	</a>
</li>
<li class="nav-item has-treeview">
	<a href="#" class="nav-link">
		<i class="nav-icon fas fa-book"></i>
		<p>
			Sepatu
			<i class="right fas fa-angle-left"></i>
		</p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?= site_url('sepatu') ?>" class="nav-link">
				<i class="far fa-circle nav-icon"></i>
				<p>List Sepatu</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?= site_url('sepatu/buatSepatu') ?>" class="nav-link">
				<i class="far fa-circle nav-icon"></i>
				<p>Buat Sepatu</p>
			</a>
		</li>
	</ul>
</li>
<li class="nav-item has-treeview">
	<a href="#" class="nav-link">
		<i class="nav-icon fas fa-person-booth"></i>
		<p>
			Pegawai
			<i class="fas fa-angle-left right"></i>
		</p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<a href="<?= site_url('pegawai') ?>" class="nav-link">
				<i class="far fa-circle nav-icon"></i>
				<p>List Pegawai</p>
			</a>
		</li>
		<li class="nav-item">
			<a href="<?= site_url('pegawai/tambahPegawai') ?>" class="nav-link">
				<i class="far fa-circle nav-icon"></i>
				<p>Tambah Pegawai</p>
			</a>
		</li>
	</ul>
</li>
