<div class="wrap-login100 p-l-55 p-r-55 p-t-25 p-b-25">
	<form action="<?= base_url() . "Register/prosesRegister" ?>" method="post"
		  class="login100-form validate-form flex-sb flex-w">
					<span class="login100-form-title p-b-32">
						Daftar Akun
					</span>

		<span class="txt1 p-b-11">
						Username
					</span>
		<div class="wrap-input100 validate-input m-b-36" data-validate="Username is required">
			<input autocomplete="off" class="input100" type="text" name="nama">
			<span class="focus-input100"></span>
		</div>
		<!-- <span class="txt1 p-b-11">
						Email
					</span>
		<div class="wrap-input100 validate-input m-b-36" data-validate="Username is required">
			<input autocomplete="off" class="input100" type="email" name="email">
			<span class="focus-input100"></span>
		</div> -->
		<span class="txt1 p-b-11">
						Sandi
					</span>
		<div class="wrap-input100 validate-input m-b-12" data-validate="Password is required">
						<span class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
			<input class="input100" type="password" name="password">
			<span class="focus-input100"></span>
		</div>
		<div class="container-login100-form-btn">
			<button type="submit" class="login100-form-btn">
				Daftar
			</button>
		</div>
	</form>
</div>
