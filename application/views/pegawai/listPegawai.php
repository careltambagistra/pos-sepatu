<div class="col-md-12">
	<div class="card card-gray-dark">
		<div class="card-header">
			<h3 class="card-title">*</h3>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<table id="example2" class="table table-bordered table-hover">
				<thead>
				<tr>
					<th id="hidden">No</th>
					<th>Nama</th>
					<th>Tanggal lahir</th>
					<th>Alamat</th>
					<th>Jenis Kelamin</th>
					<th>Posisi</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$no = 1;
				foreach ($pegawais as $pegawai) {
					?>
					<tr>
						<td><?= $no++ ?></td>
						<td><?= $pegawai->nama_pegawai ?></td>
						<td><?= $pegawai->tanggal_lahir ?></td>
						<td><?= $pegawai->alamat ?></td>
						<td><?= $pegawai->jenis_kelamin ?></td>
						<td><?= $pegawai->posisi ?></td>
						<td>
							<a href="<?= site_url('pegawai/updatePegawai/') . $pegawai->id_pegawai ?>"
							   class="btn btn-sm bg-gradient-green">
								<i class="fas fa-edit"></i></a>
							<a href="#" data-id="<?= $pegawai->id_pegawai ?>"
							   class="btn btn-sm bg-gradient-danger btn-delete-pegawai">
								<i class="fas fa-trash"></i></a>
							<a href="<?= site_url('pegawai/detailPegawai/') . $pegawai->id_pegawai ?>" class="btn btn-sm bg-gradient-fuchsia">
								<i class="fas fa-eye"></i></a>
						</td>
					</tr>
					<?php
				}
				?>
				</tbody>
			</table>
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>
<div class="modal fade" id="modal-confirm-delete">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<h4>Anda yakin ingin hapus buku ini?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn bg-gradient-green" data-dismiss="modal">Tidak</button>
				<button type="button" class="btn bg-gradient-danger" id="btn-delete">Hapus</button>
			</div>
		</div>
	</div>
</div>
<form id="form-delete" method="post" action="<?= site_url('pegawai/prosesDelete') ?>">
</form>
<script>
	$(function () {
		let idPegawai = 0;
		$(".btn-delete-pegawai").on("click", function () {
			idPegawai = $(this).data("id");
			console.log(idPegawai);
			$("#modal-confirm-delete").modal("show");
		});
		$("#btn-delete").on("click", function () {
//panggil url untuk hapus data
			let inputId = $("<input>")
					.attr("type", "hidden")
					.attr("name", "id")
					.val(idPegawai);
			let formDelete = $("#form-delete");
			formDelete.empty().append(inputId);
			formDelete.submit();
			$("#modal-confirm-delete").modal("hide");
		});
	});
</script>
