<div class="col-md-12">
	<div class="card card-gray-dark">
		<div class="card-header">
			<h3 class="card-title">*</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<div class="card-body">
			<form action="<?= site_url('pegawai/prosesBuat') ?>" enctype="multipart/form-data" method="post">
				<div class="form-group">
					<label for="nama_pegawai">Nama</label>
					<input autocomplete="off" type="text" id="nama_pegawai" name="nama" class="form-control">
				</div>
				<div class="form-group">
					<label for="tanggal_lahir">Tempat, tanggal dan lahir</label>
					<input type="date" id="tanggal_lahir" name="tanggal" class="form-control">
				</div>
				<div class="form-group">
					<label for="alamat">Alamat</label>
					<textarea id="alamat" name="alamat" class="form-control" rows="4"></textarea>

				</div>
				<div class="form-group">
					<label for="jenis_kelamin">Jenis Kelamin</label>
					<select id="jenis_kelamin" name="kelamin" class="form-control custom-select">
						<option selected disabled>Pilih salah satu</option>
						<option>Laki-laki</option>
						<option>Perempuan</option>
					</select>
				</div>
				<div class="form-group">
					<label for="posisi">Posisi</label>
					<select id="posisi" name="posisi" class="form-control custom-select">
						<option selected disabled>Pilih salah satu</option>
						<option>Manager</option>
						<option>Karyawan</option>
					</select>
				</div>
				<div class="form-group">
					<label for="foto-pegawai">Foto</label>
					<input type="file" id="foto_pegawai" name="foto" class="form-control">
				</div>
				<div class="col-12">
					<a href="<?= site_url('pegawai') ?>" class="btn btn-secondary">Cancel</a>
					<input type="submit" value="Buat Baru" class="btn btn-success float-right">
				</div>
			</form>
		</div>
		<!-- /.card-body -->
	</div>
