<div class="col-md-12">
	<div class="card card-gray-dark">
		<div class="card-header">
			<h3 class="card-title">*</h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fas fa-minus"></i></button>
			</div>
		</div>
		<div class="card-body">
			<form action="<?= site_url('pegawai/prosesEdit') ?>" enctype="multipart/form-data" method="post">
				<div class="form-group">
					<label for="nama_pegawai">Nama Pegawai</label>
					<input autocomplete="off" type="text" id="nama_pegawai" name="nama" class="form-control" value="<?= $pegawai->nama_pegawai ?>">
				</div>
				<div class="form-group">
					<label for="tanggal_lahir">Tanggal Lahir</label>
					<input type="date" id="tanggal_lahir" name="tanggal" class="form-control" value="<?= $pegawai->tanggal_lahir ?>">
				</div>
				<div class="form-group">
					<label for="alamat">Alamat</label>
					<input autocomplete="off" id="alamat" name="alamat" class="form-control" value="<?= $pegawai->alamat ?>">

				</div>
				<div class="form-group">
					<label for="jenis_kelamin">Jenis Kelamin</label>
					<select id="jenis_kelamin" name="kelamin" class="form-control custom-select" value="<?= $pegawai->jenis_kelamin ?>">
						<option selected disabled>Pilih salah satu</option>
						<option>Laki-laki</option>
						<option>Perempuan</option>
					</select>
				</div>
				<div class="form-group">
					<label for="posisi">Posisi</label>
					<select id="posisi" name="posisi" class="form-control custom-select" value="<?= $pegawai->posisi ?>">
						<option selected disabled>Pilih salah satu</option>
						<option>Manager</option>
						<option>Karyawan</option>
					</select>
				</div>
				<div class="form-group">
					<label for="foto">Foto</label>
					<div class="clo-sm-10">
						<div class="row">
							<div class="col-sm-3">
								<img src="<?= base_url('asset/images/') . $pegawai->foto_pegawai; ?>" class="img-thumbnail">
							</div>
							<div class="form-group col-sm-9">
								<label for="stock-barang">Foto Pegawai</label>
								<input value="<?= $pegawai->foto_pegawai ?>" required type="file" name="foto" id="gambar_buku" class="form-control"/>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<a href="<?= site_url('pegawai') ?>" class="btn btn-secondary">Cancel</a>
					<input type="submit" value="Simpan" class="btn btn-success float-right">
				</div>
				<input type="hidden" id="id_pegawai" name="id" value="<?= $pegawai->id_pegawai ?>">
			</form>
		</div>
	</div>
