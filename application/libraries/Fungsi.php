<?php


class Fungsi
{
	protected $CI;

	public function __construct()
	{
		$this->CI = &get_instance();
	}
	public function count_buku(){
		$this->CI->load->model('ModelSepatu');
		return $this->CI->ModelSepatu->get()->num_rows();
	}
	public function count_pegawai(){
		$this->CI->load->model('ModelPegawai');
		return $this->CI->ModelPegawai->get()->num_rows();
	}
	public function count_transaksi(){
		$this->CI->load->model('ModelTransaksi');
		return $this->CI->ModelTransaksi->get()->num_rows();
	}
}
